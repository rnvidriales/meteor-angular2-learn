// tmeasday:publish-counts package
// @see https://github.com/percolatestudio/publish-counts
// This package is an example for a package that does not provide it's own Typings,
// so we will have to create d.ts file by our self according to the package API
interface CountsObject {
  get(publicationName: string): number;
  publish(context: any, publicationName: string, cursor: Mongo.Cursor<any>, options: any): number;
  has(publicationName: string);
  noWarnings();
}

declare module 'meteor/tmeasday:publish-counts' {
  export let Counts: CountsObject;
}
