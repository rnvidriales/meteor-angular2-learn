import { Component, OnInit } from '@angular/core';
import { REACTIVE_FORM_DIRECTIVES, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Meteor } from 'meteor/meteor';
import { MeteorComponent } from 'angular2-meteor';
import { InjectUser } from 'angular2-meteor-accounts-ui';

import { Parties } from '../../../both/collections/parties.collection';

import template from './parties-form.component.html';

@Component({
  selector: 'parties-form',
  template,
  directives: [REACTIVE_FORM_DIRECTIVES],
})
@InjectUser('user')
export class PartiesFormComponent extends MeteorComponent implements OnInit {
  // we use OnInit interface.
  // It brings the ngOnInit method.
  // It initialize the directive/component after Angular initializes the data-bound
  // input properties. Angular will find and call methods like ngOnInit(), with
  // or without the interfaces. Nonetheless, we strongly recommend adding interfaces
  // to TypeScript directive classes in order to benefit from strong typing and
  // editor tooling.
  addForm: FormGroup;
  user: Meteor.User;

  constructor(private formBuilder: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: [],
      location: ['', Validators.required],
      public: [false],
    });
  }

  resetForm() {
   this.addForm.controls['name']['updateValue']('');
   this.addForm.controls['description']['updateValue']('');
   this.addForm.controls['location']['updateValue']('');
   this.addForm.controls['public']['updateValue'](false);
 }

 addParty() {
   if (this.addForm.valid) {
      if (Meteor.userId()) {
        Parties.insert({
          name: this.addForm.value.name,
          description: this.addForm.value.description,
          location: {
            name: this.addForm.value.location,
          },
          public: this.addForm.value.public,
          owner: Meteor.userId(),
        });

        // XXX will be replaced by this.addForm.reset() in RC5+
        this.resetForm();
      } else {
        alert('Please log in to add a party');
      }
   }
 }
}
